import { defineManifest } from "@crxjs/vite-plugin";
import packageJson from "./package.json";
const { version } = packageJson;

const [major, minor, patch, label = "0"] = version
  .replace(/[^\d.-]+/g, "")
  .split(/[.-]/);

export default defineManifest(async (env) => ({
  manifest_version: 3,
  name: (env.mode === "development" ? "[DEV]" : "") + "filter player",
  version: `${major}.${minor}.${patch}.${label}`,
  version_name: version,
  description: "Create a filter in wgsl, apply it to an image or video, and save it as an image or animated image.",

  icons: {
    16: "src/assets/icons/16.png",
    32: "src/assets/icons/32.png",
    48: "src/assets/icons/48.png",
    128: "src/assets/icons/128.png",
  },

  permissions: ["activeTab", "contextMenus", "scripting", "storage"],

  action: {
    default_icon: {
      16: "src/assets/icons/16.png",
      32: "src/assets/icons/32.png",
      48: "src/assets/icons/48.png",
      128: "src/assets/icons/128.png",
    },
  },

  background: {
    service_worker: "src/background/index.ts",
    type: "module",
  },

  options_page: "src/option/index.html",

  content_security_policy: {
    extension_pages: "script-src 'self' 'wasm-unsafe-eval'; object-src 'self';",
  },
}));
