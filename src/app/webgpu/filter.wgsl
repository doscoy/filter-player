@vertex
fn vertMain(@location(0) pos: vec2f) -> @builtin(position) vec4f {
  return vec4f(pos, 0, 1);
}
@fragment
fn main(@builtin(position) pos: vec4f) -> @location(0) vec4f {
  let uv = vec2f(pos.x, frame.dstRes.y - pos.y) / frame.dstRes;
  return textureSampleBaseClampToEdge(source, nearestSampler, uv);
}
