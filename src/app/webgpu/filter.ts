import { ExternalTexture } from "./texture";
import { Sampler } from "./sampler";
import { UniformData, type UniformDataRecipe } from "./uniform";
import { makePanelVertex } from "./vertex";
import BaseShader from "./filter.wgsl?raw";
import type { Context } from "./context";

interface ShaderUnits {
  readonly pipeline: GPURenderPipeline[];
  readonly bind: (bindGroupLayout: GPUBindGroupLayout) => GPUBindGroup;
}

interface ErrorMessage {
  readonly type: GPUCompilationMessageType;
  readonly row: number;
  readonly col: number;
  readonly len: number;
  readonly msg: string;
}

interface CompileResult {
  readonly success: boolean;
  readonly msgs: ErrorMessage[];
}

function makeBaseColorTargetState(): Iterable<GPUColorTargetState> {
  return [
    {
      format: "rgba8unorm",
    },
  ];
}

class Filter {
  private device_: GPUDevice;
  private ctx_: Context;

  private source_: ExternalTexture;
  private linearSampler_: Sampler;
  private nearestSampler_: Sampler;
  private baseUni_: UniformData;

  private vertBuf_: GPUBuffer;
  private vertState_: GPUVertexState;

  private shaderUnits_: [ShaderUnits, ShaderUnits];
  private unitsCur_: 0 | 1;

  private paramUni_: UniformData | null;

  private req_: number = 0;

  constructor(device: GPUDevice, ctx: Context, initFrame: VideoFrame) {
    this.device_ = device;
    this.ctx_ = ctx;

    this.source_ = new ExternalTexture(this.device_, "source", initFrame);
    this.linearSampler_ = new Sampler(this.device_, "linearSampler", {
      magFilter: "linear",
      minFilter: "linear",
    });
    this.nearestSampler_ = new Sampler(this.device_, "nearestSampler", {
      magFilter: "nearest",
      minFilter: "nearest",
    });
    this.baseUni_ = new UniformData(this.device_, "frame", {
      srcRes: "vec2f",
      dstRes: "vec2f",
      timestamp: "f32",
    });
    {
      const { width, height } = this.ctx_.getTexture().getSrcSide();
      this.baseUni_.setValue("srcRes", [
        initFrame.displayWidth,
        initFrame.displayHeight,
      ]);
      this.baseUni_.setValue("dstRes", [width, height]);
    }

    const [vertBuf, vertLayout] = makePanelVertex(this.device_);
    this.vertBuf_ = vertBuf;
    const shader = this.device_.createShaderModule({
      code: this.makeBaseUniformSrcStr() + BaseShader,
    });
    this.vertState_ = {
      module: shader,
      entryPoint: "vertMain",
      buffers: [vertLayout],
    };

    const pipeline = this.device_.createRenderPipeline({
      label: "filter:basePipeline",
      layout: this.device_.createPipelineLayout({
        bindGroupLayouts: [
          this.device_.createBindGroupLayout(
            this.makeBaseBindGroupLayoutDesc()
          ),
        ],
      }),
      vertex: this.vertState_,
      fragment: {
        module: shader,
        entryPoint: "main",
        targets: makeBaseColorTargetState(),
      },
    });
    const bind = (bindGroupLayout: GPUBindGroupLayout) =>
      this.device_.createBindGroup(this.makeBaseBindGroupDesc(bindGroupLayout));
    this.shaderUnits_ = [
      { pipeline: [pipeline], bind },
      { pipeline: [pipeline], bind },
    ];
    this.unitsCur_ = 0;

    this.paramUni_ = null;
  }

  get enable(): boolean {
    return this.unitsCur_ === 1;
  }

  set enable(flag: boolean) {
    this.unitsCur_ = flag ? 1 : 0;
  }

  updateFrame(frame: VideoFrame, timestamp?: number) {
    this.source_.setFrame(frame);
    if (!timestamp) timestamp = frame.timestamp / 1_000_000;
    this.baseUni_.setValue("srcRes", [frame.displayWidth, frame.displayHeight]);
    this.baseUni_.setValue("timestamp", [timestamp]);
  }

  resize(size: [number, number]) {
    const { width, height } = this.ctx_.getTexture().getSrcSide();
    if (size[0] !== width || size[1] !== height) {
      this.ctx_.resize(size);
      this.baseUni_.setValue("dstRes", size);
    }
  }

  async setShader(
    src: string,
    uniform?: UniformDataRecipe
  ): Promise<CompileResult> {
    const binding = 5;
    const uni =
      uniform && Object.keys(uniform).length !== 0
        ? new UniformData(this.device_, "param", uniform)
        : null;
    const uniSrc =
      this.makeBaseUniformSrcStr() + (uni ? uni.makeSrcStr(0, binding) : "");

    const entryPoints = [
      ...src.matchAll(/^@fragment[\n ]+fn[\n ]+([\w]+)/gim),
    ].map(([_, id]) => id);

    this.device_.pushErrorScope("validation");
    const shader = this.device_.createShaderModule({
      code: uniSrc + src,
    });
    this.device_.popErrorScope();

    const { messages } = await shader.getCompilationInfo();
    const res = (() => {
      const rowOffset = uniSrc.split("\n").length - 1;
      const msgs = messages.map((msg: GPUCompilationMessage) => ({
        type: msg.type,
        row: msg.lineNum - rowOffset,
        col: msg.linePos,
        len: msg.length,
        msg: msg.message,
      }));
      return { success: messages.every((item) => item.type !== "error"), msgs };
    })();
    if (!res.success) {
      return res;
    }

    if (this.paramUni_) this.paramUni_.dispose();
    this.paramUni_ = uni;

    const bindGroupLayout = (() => {
      const desc = this.makeBaseBindGroupLayoutDesc();
      if (this.paramUni_) {
        desc.entries = [
          ...desc.entries,
          {
            binding,
            visibility: GPUShaderStage.FRAGMENT,
            buffer: {
              type: "uniform",
              minBindingSize: this.paramUni_.gpuBuffer.size,
            },
          },
        ];
      }
      return this.device_.createBindGroupLayout(desc);
    })();

    const pipeline = entryPoints.map((entryPoint, i) => {
      return this.device_.createRenderPipeline({
        label: `filter:effectPipeline:${i}`,
        layout: this.device_.createPipelineLayout({
          bindGroupLayouts: [bindGroupLayout],
        }),
        vertex: this.vertState_,
        fragment: {
          module: shader,
          entryPoint: entryPoint,
          targets: makeBaseColorTargetState(),
        },
      });
    });

    const bind = () => {
      const desc = this.makeBaseBindGroupDesc(bindGroupLayout);
      if (this.paramUni_) {
        desc.entries.push({
          binding,
          resource: { buffer: this.paramUni_.gpuBuffer },
        });
      }
      return this.device_.createBindGroup(desc);
    };

    this.shaderUnits_[1] = {
      pipeline,
      bind,
    };

    return res;
  }

  setParameter(key: string, value: number[]) {
    if (!this.paramUni_) return;
    this.paramUni_.setValue(key, value);
  }

  private makeBaseUniformSrcStr() {
    return (
      this.baseUni_.makeSrcStr(0, 0) +
      this.source_.makeSrcStr(0, 1) +
      this.ctx_.getTexture().makeSrcStr(0, 2) +
      this.linearSampler_.makeSrcStr(0, 3) +
      this.nearestSampler_.makeSrcStr(0, 4)
    );
  }

  private makeBaseBindGroupLayoutDesc(): GPUBindGroupLayoutDescriptor {
    return {
      label: "filter:base bind group layout",
      entries: [
        this.baseUni_.makeBindGroupLayoutEntry(0, ["FRAGMENT"]),
        this.source_.makeBindGroupLayoutEntry(1, ["FRAGMENT"]),
        this.ctx_.getTexture().makeBindGroupLayoutEntry(2, ["FRAGMENT"]),
        this.linearSampler_.makeBindGroupLayoutEntry(3, ["FRAGMENT"]),
        this.nearestSampler_.makeBindGroupLayoutEntry(4, ["FRAGMENT"]),
      ],
    };
  }

  private makeBaseBindGroupDesc(bindGroupLayout: GPUBindGroupLayout) {
    return {
      label: "filter:base bind group",
      layout: bindGroupLayout,
      entries: [
        this.baseUni_.makeBindGroupEntry(0),
        this.source_.makeBindGroupEntry(1),
        this.ctx_.getTexture().makeBindGroupEntry(2),
        this.linearSampler_.makeBindGroupEntry(3),
        this.nearestSampler_.makeBindGroupEntry(4),
      ],
    };
  }

  draw() {
    this.baseUni_.enqueue();
    if (this.unitsCur_ && this.paramUni_) this.paramUni_.enqueue();
    const encoder = this.device_.createCommandEncoder();
    const texture = this.ctx_.getTexture();
    const unit = this.shaderUnits_[this.unitsCur_];
    for (let i = 0; i < unit.pipeline.length; ++i) {
      const pass = encoder.beginRenderPass({
        colorAttachments: [
          {
            view: texture.getDstSide().createView(),
            loadOp: "clear",
            storeOp: "store",
          },
        ],
      });
      pass.setPipeline(unit.pipeline[i]);
      pass.setBindGroup(0, unit.bind(unit.pipeline[i].getBindGroupLayout(0)));
      pass.setVertexBuffer(0, this.vertBuf_);
      pass.draw(this.vertBuf_.size / (4 * 2));
      pass.end();
      texture.flip();
    }
    this.device_.queue.submit([encoder.finish()]);
    this.ctx_.update();
  }

  drawAtNextFrame() {
    if (this.req_ !== 0) return;
    this.req_ = window.requestAnimationFrame(() => {
      this.req_ = 0;
      this.draw();
    });
  }

  async getImageData(flipY: boolean) {
    const align = 256;
    const texture = this.ctx_.getTexture().getSrcSide();
    const bytesPerRow = texture.width * 4;
    const paddedBytesPerRow =
      bytesPerRow + ((align - (bytesPerRow % align)) % align);
    const buffer = this.device_.createBuffer({
      size: paddedBytesPerRow * texture.height,
      usage: GPUBufferUsage.MAP_READ | GPUBufferUsage.COPY_DST,
    });

    const encoder = this.device_.createCommandEncoder();
    encoder.copyTextureToBuffer(
      { texture },
      {
        buffer,
        bytesPerRow: paddedBytesPerRow,
        rowsPerImage: texture.height,
      },
      [texture.width, texture.height]
    );
    this.device_.queue.submit([encoder.finish()]);

    await buffer.mapAsync(GPUMapMode.READ);
    const src = new Uint8ClampedArray(buffer.getMappedRange());
    const dst = new ImageData(texture.width, texture.height);
    if (flipY) {
      for (let i = 0, end = texture.height; i < end; ++i) {
        const slice = src
          .slice(i * paddedBytesPerRow, (i + 1) * paddedBytesPerRow)
          .slice(0, bytesPerRow);
        dst.data.set(slice, (end - i - 1) * bytesPerRow);
      }
    } else {
      if (bytesPerRow === paddedBytesPerRow) {
        dst.data.set(src.slice());
      } else {
        for (let i = 0, end = texture.height; i < end; ++i) {
          const slice = src
            .slice(i * paddedBytesPerRow, (i + 1) * paddedBytesPerRow)
            .slice(0, bytesPerRow);
          dst.data.set(slice, i * bytesPerRow);
        }
      }
    }
    buffer.unmap();
    buffer.destroy();

    return dst;
  }

  dispose() {
    if (this.req_ !== 0) window.cancelAnimationFrame(this.req_);
    this.source_.dispose();
    this.linearSampler_.dispose();
    this.nearestSampler_.dispose();
    this.baseUni_.dispose();
    this.vertBuf_.destroy();
    if (this.paramUni_) this.paramUni_.dispose();
  }
}

export default Filter;
