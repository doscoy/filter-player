struct VertexOutput {
  @builtin(position) pos: vec4f,
  @location(0) uv: vec2f,
};

@vertex
fn vertMain(@location(0) pos: vec2f) -> VertexOutput {
  return VertexOutput(
    vec4f(
      (pos * uni.canvasRes * uni.zoom + uni.offset * 2) / uni.worldRes,
      0.0,
      1.0),
    vec2f((pos + 1.0) * 0.5),
  );
}

@fragment
fn fragMain(@location(0) uv: vec2f) -> @location(0) vec4f {
  return textureSample(canvas, canvasSampler, uv);
}
