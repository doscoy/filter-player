import type { FlipTexture } from "./texture";

interface Context {
  getTexture(): FlipTexture;
  resize(size: [number, number]): void;
  update(): void;
}

export type { Context };
