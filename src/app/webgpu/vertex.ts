function makePanelVertex(
  device: GPUDevice,
  location: GPUIndex32 = 0
): [GPUBuffer, GPUVertexBufferLayout] {
  const data = new Float32Array([
    -1.0, -1.0, 1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0,
  ]);
  const buffer = device.createBuffer({
    size: data.byteLength,
    usage: GPUBufferUsage.VERTEX | GPUBufferUsage.COPY_DST,
  });
  device.queue.writeBuffer(buffer, 0, data);
  const layout: GPUVertexBufferLayout = {
    arrayStride: 8,
    attributes: [
      {
        format: "float32x2",
        offset: 0,
        shaderLocation: location,
      },
    ],
  };
  return [buffer, layout];
}

export { makePanelVertex };
