import type { GPUShaderStageKey, BindBase } from "./bindbase";

type UniformValueTypes = "f32" | "vec2f" | "vec3f" | "vec4f";

interface UniformDataRecipe {
  [K: string]: UniformValueTypes;
}

const SIZEMAP: { [K in UniformValueTypes]: number } = {
  f32: 1,
  vec2f: 2,
  vec3f: 3,
  vec4f: 4,
};

const ALIGNMAP: { [K in UniformValueTypes]: number } = {
  f32: 4,
  vec2f: 8,
  vec3f: 16,
  vec4f: 16,
};

function calcPaddedOffset(pos: number, align: number) {
  return Math.ceil(pos / align) * align;
}

function parseRecipe(
  name: string,
  recipe: UniformDataRecipe
): [
  buffer: ArrayBuffer,
  subArray: Record<string, Float32Array>,
  structSrc: string
] {
  let offset = 0;
  let str = `struct ${name}{`;
  const subLenMemo: { [K: string]: { offset: number; length: number } } = {};
  for (const key in recipe) {
    const type = recipe[key];
    const subLen = SIZEMAP[type];
    const align = ALIGNMAP[type];
    offset = calcPaddedOffset(offset, align);
    subLenMemo[key] = { offset, length: subLen };
    str += `${key}:${type},`;
    offset += subLen * 4;
  }
  str += "};";
  const buffer = new ArrayBuffer(offset);
  const subArray: Record<string, Float32Array> = {};
  for (const key in subLenMemo) {
    subArray[key] = new Float32Array(
      buffer,
      subLenMemo[key].offset,
      subLenMemo[key].length
    );
  }
  return [buffer, subArray, str];
}

class UniformData implements BindBase {
  private device_: GPUDevice;
  private name_: string;
  private localBuf_: ArrayBuffer;
  private mapping_: Record<string, Float32Array>;
  private structSrc_: string;
  private gpuBuf_: GPUBuffer;
  private update_: boolean;

  constructor(device: GPUDevice, name: string, recipe: UniformDataRecipe) {
    this.device_ = device;
    this.name_ = name;
    const [buffer, subArray, structSrc] = parseRecipe(this.structName_, recipe);
    this.localBuf_ = buffer;
    this.mapping_ = subArray;
    this.structSrc_ = structSrc;
    this.gpuBuf_ = this.device_.createBuffer({
      label: `uniform:${name}`,
      size: calcPaddedOffset(
        buffer.byteLength,
        buffer.byteLength <= 8 ? 8 : 16
      ),
      usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST,
    });
    this.update_ = false;
  }

  private get structName_() {
    return `${this.name_}_`;
  }

  get name() {
    return this.name_;
  }

  get gpuBuffer() {
    return this.gpuBuf_;
  }

  setValue(key: string, value: readonly number[]) {
    const arr = this.mapping_[key];
    if (!arr) return;
    for (let i = 0; i < arr.length; ++i) arr[i] = value[i];
    this.update_ = true;
  }

  getValue(key: string) {
    const arr = this.mapping_[key];
    if (!arr) return [];
    return Array.from(arr);
  }

  getKeys() {
    return Object.keys(this.mapping_);
  }

  makeBindGroupLayoutEntry(
    binding: GPUIndex32,
    visibility: GPUShaderStageKey[]
  ): GPUBindGroupLayoutEntry {
    return {
      binding,
      visibility: visibility.reduce((a, k) => a | GPUShaderStage[k], 0),
      buffer: {
        type: "uniform",
        minBindingSize: this.gpuBuf_.size,
      },
    };
  }

  makeBindGroupEntry(binding: GPUIndex32): GPUBindGroupEntry {
    return {
      binding: binding,
      resource: { buffer: this.gpuBuf_ },
    };
  }

  makeSrcStr(group: GPUIndex32, binding: GPUIndex32) {
    return `${this.structSrc_}@group(${group})@binding(${binding})var<uniform>${this.name_}:${this.structName_};\n`;
  }

  enqueue() {
    if (!this.update_) return;
    this.device_.queue.writeBuffer(this.gpuBuf_, 0, this.localBuf_);
    this.update_ = false;
  }

  dispose() {
    this.name_ = "";
    this.gpuBuf_.destroy();
  }
}

export { UniformData, SIZEMAP };
export type { UniformValueTypes, UniformDataRecipe };
