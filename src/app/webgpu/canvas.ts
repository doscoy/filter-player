import { FlipTexture } from "./texture";
import { Sampler } from "./sampler";
import { UniformData } from "./uniform";
import { makePanelVertex } from "./vertex";
import CanvasWGSL from "./canvas.wgsl?raw";
import type { Context } from "./context";

function normalizedMousePos(e: MouseEvent) {
  return [];
}

class Canvas {
  private elm_: HTMLCanvasElement;
  private device_: GPUDevice;
  private ctx_: GPUCanvasContext;
  private clearColor_: GPUColor;

  private canvas_: FlipTexture;
  private sampler_: Sampler;
  private uni_: UniformData;
  private vertBuf_: GPUBuffer;

  private pipeline_: GPURenderPipeline;

  private resize_: ResizeObserver;
  private pxRatioHandler_: () => void;

  private req_: number;

  constructor(
    parent: HTMLElement,
    device: GPUDevice,
    [initSizeX, initSizeY]: [number, number] = [1, 1],
    [r, g, b, a]: [number, number, number, number] = [0, 0, 0, 1]
  ) {
    this.elm_ = parent.appendChild(document.createElement("canvas"));
    this.elm_.style.width = "100%";
    this.elm_.style.height = "100%";
    this.elm_.width = this.elm_.clientWidth * devicePixelRatio;
    this.elm_.height = this.elm_.clientHeight * devicePixelRatio;

    this.device_ = device;

    this.ctx_ = this.elm_.getContext("webgpu")!;
    this.ctx_.configure({
      device: this.device_,
      format: navigator.gpu.getPreferredCanvasFormat(),
    });

    this.clearColor_ = { r, g, b, a };

    this.canvas_ = new FlipTexture(this.device_, "canvas", {
      size: [initSizeX, initSizeY, 1],
      usage:
        GPUTextureUsage.RENDER_ATTACHMENT |
        GPUTextureUsage.TEXTURE_BINDING |
        GPUTextureUsage.COPY_SRC,
      format: "rgba8unorm",
    });
    this.sampler_ = new Sampler(this.device_, "canvasSampler", {
      magFilter: "nearest",
      minFilter: "linear",
    });

    this.uni_ = new UniformData(this.device_, "uni", {
      worldRes: "vec2f",
      canvasRes: "vec2f",
      offset: "vec2f",
      zoom: "f32",
    });
    this.uni_.setValue("worldRes", [this.elm_.width, this.elm_.height]);
    {
      const { width, height } = this.canvas_.getSrcSide();
      this.uni_.setValue("canvasRes", [width, height]);
    }
    this.uni_.setValue("offset", [0, 0]);
    this.uni_.setValue("zoom", [1]);

    const [vertBuf, vertLayout] = makePanelVertex(this.device_);
    this.vertBuf_ = vertBuf;
    const code =
      this.uni_.makeSrcStr(0, 0) +
      this.canvas_.makeSrcStr(0, 1) +
      this.sampler_.makeSrcStr(0, 2) +
      CanvasWGSL;
    const shader = this.device_.createShaderModule({ code });

    this.pipeline_ = this.device_.createRenderPipeline({
      layout: this.device_.createPipelineLayout({
        bindGroupLayouts: [
          this.device_.createBindGroupLayout({
            entries: [
              this.uni_.makeBindGroupLayoutEntry(0, ["VERTEX"]),
              this.canvas_.makeBindGroupLayoutEntry(1, ["FRAGMENT"]),
              this.sampler_.makeBindGroupLayoutEntry(2, ["FRAGMENT"]),
            ],
          }),
        ],
      }),
      vertex: {
        module: shader,
        entryPoint: "vertMain",
        buffers: [vertLayout],
      },
      fragment: {
        module: shader,
        entryPoint: "fragMain",
        targets: [
          {
            format: navigator.gpu.getPreferredCanvasFormat(),
            blend: {
              color: {
                srcFactor: "src-alpha",
                dstFactor: "one-minus-src-alpha",
                operation: "add",
              },
              alpha: {
                srcFactor: "one",
                dstFactor: "zero",
                operation: "add",
              },
            },
          },
        ],
      },
    });

    this.elm_.addEventListener("mousedown", (e) => this.click(e));
    this.elm_.addEventListener("wheel", (e) => this.wheel(e), {
      passive: false,
    });
    this.elm_.addEventListener("contextmenu", (e) => e.preventDefault());

    this.resize_ = new ResizeObserver(() => this.resizeWorld());
    this.resize_.observe(this.elm_);

    this.pxRatioHandler_ = () => {
      this.resizeWorld();
      matchMedia(`(resolution: ${devicePixelRatio}dppx)`).addEventListener(
        "change",
        this.pxRatioHandler_,
        { once: true }
      );
    };

    this.req_ = 0;

    this.fitWorld();
  }

  draw() {
    if (this.req_ !== 0) return;
    this.req_ = window.requestAnimationFrame(() => {
      this.req_ = 0;
      if (!this.ctx_.canvas.width || !this.ctx_.canvas.height) return;
      this.uni_.enqueue();
      const encoder = this.device_.createCommandEncoder();
      const pass = encoder.beginRenderPass({
        colorAttachments: [
          {
            view: this.ctx_.getCurrentTexture().createView(),
            clearValue: this.clearColor_,
            loadOp: "clear",
            storeOp: "store",
          },
        ],
      });
      pass.setPipeline(this.pipeline_);
      pass.setVertexBuffer(0, this.vertBuf_);
      const bind = this.createBindGroup();
      pass.setBindGroup(0, bind);
      pass.draw(this.vertBuf_.size / (4 * 2));
      pass.end();
      this.device_.queue.submit([encoder.finish()]);
    });
  }

  setCanvasSize(size: [number, number]) {
    this.canvas_.resize(size.map((n) => Math.max(1, n)));
    const { width, height } = this.canvas_.getSrcSide();
    this.uni_.setValue("canvasRes", [width, height]);
  }

  fitWorld() {
    const [worldW, worldH] = this.uni_.getValue("worldRes");
    const [canvasW, canvasH] = this.uni_.getValue("canvasRes");
    this.uni_.setValue("zoom", [Math.min(worldW / canvasW, worldH / canvasH)]);
    this.uni_.setValue("offset", [0, 0]);
    this.draw();
  }

  fitCanvas(e: MouseEvent) {
    const [zoom] = this.uni_.getValue("zoom");
    const offset = this.uni_.getValue("offset");
    const world = this.uni_.getValue("worldRes");
    const canvas = this.uni_.getValue("canvasRes");
    const mouse = this.normalizedMousePos(e);
    this.uni_.setValue(
      "offset",
      Array.from({ length: 2 }, (_, i) => {
        const val = (world[i] < canvas[i] ? offset[i] - mouse[i] : 0) / zoom;
        const thr = canvas[i] * 0.5;
        return Math.min(Math.max(-thr, val), thr);
      })
    );
    this.uni_.setValue("zoom", [1]);
    this.draw();
  }

  getContext(): Context {
    return {
      getTexture: () => this.canvas_,
      resize: (size: [number, number]) => this.setCanvasSize(size),
      update: () => this.draw(),
    };
  }

  private createBindGroup() {
    return this.device_.createBindGroup({
      layout: this.pipeline_.getBindGroupLayout(0),
      entries: [
        this.uni_.makeBindGroupEntry(0),
        this.canvas_.makeBindGroupEntry(1),
        this.sampler_.makeBindGroupEntry(2),
      ],
    });
  }

  private resizeWorld() {
    this.elm_.width = this.elm_.clientWidth * devicePixelRatio;
    this.elm_.height = this.elm_.clientHeight * devicePixelRatio;
    this.uni_.setValue("worldRes", [this.elm_.width, this.elm_.height]);
    this.draw();
  }

  private normalizedMousePos(e: MouseEvent) {
    return [
      e.clientX * devicePixelRatio - this.elm_.width * 0.5,
      this.elm_.height * 0.5 - e.clientY * devicePixelRatio,
    ];
  }

  private click(e: MouseEvent) {
    switch (e.button) {
      case 0:
        this.drag();
        break;
      case 1:
        this.fitWorld();
        break;
      case 2:
        this.fitCanvas(e);
        break;
      default:
      // do nothing
    }
  }

  private drag() {
    const dragHandler = (e: MouseEvent) => {
      const offset = this.uni_.getValue("offset");
      const [zoom] = this.uni_.getValue("zoom");
      const canvas = this.uni_.getValue("canvasRes");
      const move = [
        e.movementX * devicePixelRatio,
        -e.movementY * devicePixelRatio,
      ];
      this.uni_.setValue(
        "offset",
        Array.from({ length: 2 }, (_, i) => {
          const val = offset[i] + move[i];
          const thr = canvas[i] * zoom * 0.5;
          return Math.min(Math.max(-thr, val), thr);
        })
      );
      this.draw();
    };
    this.elm_.addEventListener("mousemove", dragHandler);
    this.elm_.addEventListener(
      "mouseup",
      () => this.elm_.removeEventListener("mousemove", dragHandler),
      { once: true }
    );
    this.elm_.addEventListener(
      "mouseleave",
      () => this.elm_.removeEventListener("mousemove", dragHandler),
      { once: true }
    );
  }

  private wheel(e: WheelEvent) {
    e.preventDefault();

    const [zoom] = this.uni_.getValue("zoom");
    if (e.deltaY === 0) return;
    const newZoom = Math.max(zoom * (e.deltaY < 0 ? 1.1 : 0.9), 0.0001);
    this.uni_.setValue("zoom", [newZoom]);

    const offset = this.uni_.getValue("offset");
    const canvas = this.uni_.getValue("canvasRes");
    const mouse = this.normalizedMousePos(e);
    this.uni_.setValue(
      "offset",
      Array.from({ length: 2 }, (_, i) => {
        const val = mouse[i] + ((offset[i] - mouse[i]) * newZoom) / zoom;
        const thr = canvas[i] * zoom * 0.5;
        return Math.min(Math.max(-thr, val), thr);
      })
    );
    this.draw();
  }

  dispose() {
    if (this.req_ !== 0) window.cancelAnimationFrame(this.req_);
    this.req_ = 0;
    this.canvas_.dispose();
    this.sampler_.dispose();
    this.uni_.dispose();
    this.vertBuf_.destroy();
    this.resize_.disconnect();
    matchMedia(`(resolution: ${devicePixelRatio}dppx)`).removeEventListener(
      "change",
      this.pxRatioHandler_
    );
  }
}

export default Canvas;
