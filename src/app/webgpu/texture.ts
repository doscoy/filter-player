import type { GPUShaderStageKey, BindBase } from "./bindbase";

class Texture implements BindBase {
  private device_: GPUDevice;
  private name_: string;
  private texture_: GPUTexture;

  constructor(device: GPUDevice, name: string, desc: GPUTextureDescriptor) {
    this.device_ = device;
    this.name_ = name;
    this.texture_ = this.device_.createTexture({
      label: `texture:${name}`,
      ...desc,
    });
  }

  get name() {
    return this.name_;
  }

  get() {
    return this.texture_;
  }

  resize(size: GPUExtent3DStrict) {
    const newDesc: GPUTextureDescriptor = {
      label: this.texture_.label,
      size,
      format: this.texture_.format,
      usage: this.texture_.usage,
      mipLevelCount: this.texture_.mipLevelCount,
      sampleCount: this.texture_.sampleCount,
      dimension: this.texture_.dimension,
    };
    this.texture_.destroy();
    this.texture_ = this.device_.createTexture(newDesc);
  }

  makeBindGroupLayoutEntry(
    binding: GPUIndex32,
    visibility: GPUShaderStageKey[] = ["FRAGMENT"]
  ): GPUBindGroupLayoutEntry {
    return {
      binding,
      visibility: visibility.reduce((a, k) => a | GPUShaderStage[k], 0),
      texture: {},
    };
  }

  makeBindGroupEntry(binding: GPUIndex32): GPUBindGroupEntry {
    return {
      binding,
      resource: this.texture_.createView(),
    };
  }

  makeSrcStr(group: GPUIndex32, binding: GPUIndex32) {
    return `@group(${group})@binding(${binding})var ${this.name_}:texture_${this.texture_.dimension}<f32>;\n`;
  }

  dispose(): void {
    this.name_ = "";
    this.texture_.destroy();
  }
}

class FlipTexture implements BindBase {
  private device_: GPUDevice;
  private name_: string;
  private texture_: [GPUTexture, GPUTexture];
  private cursor_: 0 | 1;

  constructor(device: GPUDevice, name: string, desc: GPUTextureDescriptor) {
    this.device_ = device;
    this.name_ = name;
    this.texture_ = [
      this.device_.createTexture({
        label: `flipTexture:${name}:0`,
        ...desc,
      }),
      this.device_.createTexture({
        label: `flipTexture:${name}:1`,
        ...desc,
      }),
    ];
    this.cursor_ = 0;
  }

  get name() {
    return this.name_;
  }

  getDstSide() {
    return this.texture_[this.cursor_];
  }

  getSrcSide() {
    return this.texture_[this.cursor_ ? 0 : 1];
  }

  flip() {
    this.cursor_ = this.cursor_ ? 0 : 1;
  }

  resize(size: GPUExtent3DStrict) {
    const newDescs = this.texture_.map<GPUTextureDescriptor>((tex) => {
      const ret = {
        label: tex.label,
        size,
        format: tex.format,
        usage: tex.usage,
        mipLevelCount: tex.mipLevelCount,
        sampleCount: tex.sampleCount,
        dimension: tex.dimension,
      };
      tex.destroy();
      return ret;
    });
    this.texture_ = [
      this.device_.createTexture(newDescs[0]),
      this.device_.createTexture(newDescs[1]),
    ];
  }

  makeBindGroupLayoutEntry(
    binding: GPUIndex32,
    visibility: GPUShaderStageKey[] = ["FRAGMENT"]
  ): GPUBindGroupLayoutEntry {
    return {
      binding,
      visibility: visibility.reduce((a, k) => a | GPUShaderStage[k], 0),
      texture: {},
    };
  }

  makeBindGroupEntry(binding: GPUIndex32): GPUBindGroupEntry {
    return {
      binding,
      resource: this.getSrcSide().createView(),
    };
  }

  makeSrcStr(group: GPUIndex32, binding: GPUIndex32) {
    return `@group(${group})@binding(${binding})var ${this.name_}:texture_${
      this.getSrcSide().dimension
    }<f32>;\n`;
  }

  dispose(): void {
    this.name_ = "";
    this.texture_.forEach((tex) => tex.destroy());
  }
}

class ExternalTexture implements BindBase {
  private device_: GPUDevice;
  private name_: string;
  private frame_: VideoFrame;
  private buffer_: VideoFrame | null;

  constructor(device: GPUDevice, name: string, initFrame: VideoFrame) {
    this.device_ = device;
    this.name_ = name;
    this.frame_ = initFrame;
    this.buffer_ = null;
  }

  get name() {
    return this.name_;
  }

  setFrame(frame: VideoFrame) {
    if (frame === this.frame_) return;
    if (this.buffer_) {
      this.buffer_.close();
    }
    this.buffer_ = this.frame_;
    this.frame_ = frame;
  }

  makeBindGroupLayoutEntry(
    binding: GPUIndex32,
    visibility: GPUShaderStageKey[] = ["FRAGMENT"]
  ): GPUBindGroupLayoutEntry {
    return {
      binding,
      visibility: visibility.reduce((a, k) => a | GPUShaderStage[k], 0),
      externalTexture: {},
    };
  }

  makeBindGroupEntry(binding: GPUIndex32): GPUBindGroupEntry {
    return {
      binding: binding,
      resource: this.device_.importExternalTexture({
        label: `externalTexture: ${this.name_}`,
        source: this.frame_,
      }),
    };
  }

  makeSrcStr(group: GPUIndex32, binding: GPUIndex32) {
    return `@group(${group})@binding(${binding})var ${this.name_}:texture_external;\n`;
  }

  dispose(): void {
    this.name_ = "";
    this.frame_.close();
    if (this.buffer_) this.buffer_.close();
  }
}

export { Texture, FlipTexture, ExternalTexture };
