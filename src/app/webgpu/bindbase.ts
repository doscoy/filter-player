type GPUShaderStageKey = keyof Omit<typeof GPUShaderStage, "prototype">;

interface BindBase {
  get name(): string;

  makeBindGroupLayoutEntry(
    binding: GPUIndex32,
    visibility: GPUShaderStageKey[]
  ): GPUBindGroupLayoutEntry;

  makeBindGroupEntry(binding: GPUIndex32): GPUBindGroupEntry;

  makeSrcStr(group: GPUIndex32, binding: GPUIndex32): string;

  dispose(): void;
}

export type { GPUShaderStageKey, BindBase };
