import type { GPUShaderStageKey, BindBase } from "./bindbase";

class Sampler implements BindBase {
  private name_: string;
  private sampler_: GPUSampler;

  constructor(device: GPUDevice, name: string, desc: GPUSamplerDescriptor) {
    this.name_ = name;
    this.sampler_ = device.createSampler({
      label: `sampler:${name}`,
      ...desc,
    });
  }

  get name() {
    return this.name_;
  }

  get() {
    return this.sampler_;
  }

  makeBindGroupLayoutEntry(
    binding: GPUIndex32,
    visibility: GPUShaderStageKey[] = ["FRAGMENT"]
  ): GPUBindGroupLayoutEntry {
    return {
      binding,
      visibility: visibility.reduce((a, k) => a | GPUShaderStage[k], 0),
      sampler: {},
    };
  }

  makeBindGroupEntry(binding: GPUIndex32): GPUBindGroupEntry {
    return {
      binding,
      resource: this.sampler_,
    };
  }

  makeSrcStr(group: GPUIndex32, binding: GPUIndex32) {
    return `@group(${group})@binding(${binding})var ${this.name_}:sampler;\n`;
  }

  dispose(): void {
    this.name_ = "";
  }
}

export { Sampler };
