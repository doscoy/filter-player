function calcDigit(n: number) {
  if (!Number.isFinite(n)) return 0;
  let digit = 1;
  while (true) {
    if (n / Math.pow(10, digit) < 1) return digit;
    ++digit;
  }
}

function formatTime(sec: number): string {
  if (!Number.isFinite(sec)) return "...";
  sec = Math.max(sec, 0);
  const minutes = Math.floor(sec / 60);
  const seconds = Math.floor(sec % 60);
  return `${minutes}:${seconds < 10 ? `0${seconds}` : seconds}`;
}

function makeFormatFrame(len: number) {
  return (frame: number) => {
    if (!Number.isFinite(frame)) return "...";
    return `${Math.floor(frame)}`.padStart(len, "0");
  };
}

export { calcDigit, formatTime, makeFormatFrame };
