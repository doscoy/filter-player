const ID_RIFF = 0x52494646; // "RIFF"
const ID_WEBP = 0x57454250; // "WEBP"
const ID_VP8X = 0x56503858; // "VP8X"
const ID_VP8_ = 0x56503820; // "VP8 "
const ID_VP8L = 0x5650384c; // "VP8L"
const ID_ANIM = 0x414e494d; // "ANIM"
const ID_ANMF = 0x414e4d46; // "ANMF"

interface VP8XData {
  icc: boolean;
  alpha: boolean;
  exif: boolean;
  xmp: boolean;
  animation: boolean;
  width: number;
  height: number;
}

interface ANMFData {
  xOffset: number;
  yOffset: number;
  width: number;
  height: number;
  delayMs: number;
  blending: boolean;
  dispose: boolean;
}

interface Options {
  xOffset: number;
  yOffset: number;
  blending: boolean;
  dispose: boolean;
}

function readUint24LE(view: DataView, offset: number) {
  return (
    view.getUint8(offset) |
    (view.getUint8(offset + 1) << 8) |
    (view.getUint8(offset + 2) << 16)
  );
}

function writeUint24LE(view: DataView, offset: number, value: number) {
  view.setUint8(offset, value & 0xff);
  view.setUint8(offset + 1, (value >>> 8) & 0xff);
  view.setUint8(offset + 2, (value >>> 16) & 0xff);
}

// @ts-ignore
function readVP8X(bin: ArrayBuffer): VP8XData | undefined {
  const view = new DataView(bin);
  if (
    view.getUint32(0) !== 0x52494646 ||
    view.getUint32(8) !== 0x57454250 ||
    view.getUint32(12) !== ID_VP8X
  ) {
    return;
  } else {
    const flags = view.getUint8(20);
    return {
      icc: (flags & 0b00100000) != 0,
      alpha: (flags & 0b00010000) != 0,
      exif: (flags & 0b00001000) != 0,
      xmp: (flags & 0b00000100) != 0,
      animation: (flags & 0b00000010) != 0,
      width: readUint24LE(view, 24) + 1,
      height: readUint24LE(view, 27) + 1,
    };
  }
}

function makeFileHeader(size: number): ArrayBuffer {
  const buf = new ArrayBuffer(12);
  const view = new DataView(buf);
  view.setUint32(0, ID_RIFF);
  view.setUint32(4, size + 4, true);
  view.setUint32(8, ID_WEBP);
  return buf;
}

function makeVP8X(data: VP8XData): ArrayBuffer {
  const buf = new ArrayBuffer(18);
  const view = new DataView(buf);
  view.setUint32(0, ID_VP8X);
  view.setUint32(4, buf.byteLength - 8, true);
  view.setUint8(
    8,
    (data.icc ? 0b00100000 : 0) |
      (data.alpha ? 0b00010000 : 0) |
      (data.exif ? 0b00001000 : 0) |
      (data.xmp ? 0b00000100 : 0) |
      (data.animation ? 0b00000010 : 0)
  );
  writeUint24LE(view, 12, data.width - 1);
  writeUint24LE(view, 15, data.height - 1);
  return buf;
}

function makeANIM(
  [r, g, b, a]: [number, number, number, number],
  loop: number
): ArrayBuffer {
  const buf = new ArrayBuffer(14);
  const view = new DataView(buf);
  view.setUint32(0, ID_ANIM);
  view.setUint32(4, buf.byteLength - 8, true);
  view.setUint8(8, b);
  view.setUint8(9, g);
  view.setUint8(10, r);
  view.setUint8(11, a);
  view.setUint16(12, loop, true);
  return buf;
}

function makeANMF(data: ANMFData, payload: Uint8Array): ArrayBuffer {
  const buf = new ArrayBuffer(24 + payload.byteLength);
  const view = new DataView(buf);
  view.setUint32(0, ID_ANMF);
  view.setUint32(4, buf.byteLength - 8, true);
  writeUint24LE(view, 8, data.xOffset >>> 1);
  writeUint24LE(view, 11, data.yOffset >>> 1);
  writeUint24LE(view, 14, data.width - 1);
  writeUint24LE(view, 17, data.height - 1);
  writeUint24LE(view, 20, data.delayMs);
  view.setUint8(
    23,
    (data.blending ? 0b00000010 : 0) | (data.dispose ? 0b00000001 : 0)
  );
  new Uint8Array(buf, 24).set(payload);
  return buf;
}

function copyVP8ChunkAsANMF(bin: ArrayBuffer, data: ANMFData): ArrayBuffer {
  const view = new DataView(bin);
  let cursor = 12;
  while (cursor < bin.byteLength) {
    const id = view.getUint32(cursor);
    const size = view.getUint32(cursor + 4, true);
    const next = cursor + Math.floor((size + 1) / 2) * 2 + 8;
    if (id === ID_VP8_ || id === ID_VP8L) {
      return makeANMF(data, new Uint8Array(bin, cursor, next - cursor));
    }
    cursor = next;
  }
  return new ArrayBuffer(0);
}

class Encoder {
  private width_: number;
  private height_: number;
  private quality_: number;
  private frames_: Promise<ArrayBuffer>[];

  constructor(width: number, height: number, quality: number = 1.0) {
    this.width_ = width;
    this.height_ = height;
    this.quality_ = quality;
    this.frames_ = [];
  }

  get width() {
    return this.width_;
  }

  get height() {
    return this.height_;
  }

  async pushImg(
    img: ImageData,
    delayMs: number,
    {
      xOffset = 0,
      yOffset = 0,
      dispose = true,
      blending = true,
    }: Partial<Options> = {}
  ) {
    const canvas = new OffscreenCanvas(img.width, img.height);
    const ctx = canvas.getContext("2d")!;
    ctx.putImageData(img, 0, 0);

    const promise = canvas
      .convertToBlob({ type: "image/webp", quality: this.quality_ })
      .then((blob) => blob.arrayBuffer())
      .then((bin) =>
        copyVP8ChunkAsANMF(bin, {
          width: img.width,
          height: img.height,
          xOffset,
          yOffset,
          delayMs,
          blending,
          dispose,
        })
      );
    this.frames_.push(promise);
  }

  async finish(loop: number = 0) {
    const frames = await Promise.all(this.frames_);
    const animation = frames.length > 1;
    const blobPart: (ArrayBuffer | Uint8Array)[] = [];
    blobPart.push(
      makeVP8X({
        icc: false,
        alpha: false,
        exif: false,
        xmp: false,
        animation,
        width: this.width_,
        height: this.height_,
      })
    );

    if (animation) {
      blobPart.push(makeANIM([0, 0, 0, 255], loop));
      blobPart.push(...frames);
    } else {
      blobPart.push(new Uint8Array(frames[0], 24));
    }

    blobPart.splice(
      0,
      0,
      makeFileHeader(blobPart.reduce((accm, item) => accm + item.byteLength, 0))
    );

    return new Blob(blobPart, { type: "image/webp" });
  }

  dispose() {}
}

export default Encoder;
