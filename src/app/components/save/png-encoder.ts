const CRCTABLE = Array.from({ length: 256 }, (_, n) => {
  for (let k = 0; k < 8; k++) {
    n = n & 1 ? 0xedb88320 ^ (n >>> 1) : n >>> 1;
  }
  return n;
});

function updataCRC(crc: number, data: Uint8Array): number {
  for (let n = 0; n < data.length; ++n) {
    crc = CRCTABLE[(crc ^ data[n]) & 0xff] ^ (crc >>> 8);
  }
  return crc;
}

function calcCRC(data: Uint8Array): number {
  return (updataCRC(0xffffffff, data) ^ 0xffffffff) >>> 0;
}

const ID_IHDR = 0x49484452; // "IHDR"
const ID_IDAT = 0x49444154; // "IDAT"
const ID_acTL = 0x6163544c; // "acTL"
const ID_fcTL = 0x6663544c; // "acTL"
const ID_fdAT = 0x66644154; // "fdAT"
const ID_PNG = new Uint8Array([0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a]);
const IEND = new Uint8Array([
  0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
]).buffer;

interface IHDRData {
  width: number;
  height: number;
  bitDepth: number;
  colorType: number;
  compressionMethod: number;
  filterMethod: number;
  interlaceMethod: number;
}

const DisposeOp = {
  none: 0,
  background: 1,
  previous: 2,
} as const;
type DisposeOpNum = (typeof DisposeOp)[keyof typeof DisposeOp];

const BlendOp = {
  source: 0,
  over: 1,
} as const;
type BlendOpNum = (typeof BlendOp)[keyof typeof BlendOp];

interface fcTLData {
  sequenceNumber: number;
  width: number;
  height: number;
  xOffset: number;
  yOffset: number;
  delayNum: number;
  delayDen: number;
  disposeOp: DisposeOpNum;
  blendOp: BlendOpNum;
}

interface Options {
  xOffset: number;
  yOffset: number;
  disposeOp: keyof typeof DisposeOp;
  blendOp: keyof typeof BlendOp;
}

interface FrameData extends Omit<fcTLData, "sequenceNumber"> {
  data: ArrayBuffer[];
}

// @ts-ignore
function readIHDR(bin: ArrayBuffer): IHDRData | undefined {
  const view = new DataView(bin);
  if (
    view.getUint32(0) !== new DataView(ID_PNG.buffer).getUint32(0) ||
    view.getUint32(4) !== new DataView(ID_PNG.buffer).getUint32(4) ||
    view.getUint32(12) !== ID_IHDR ||
    bin.byteLength < 28
  ) {
    return;
  } else {
    return {
      width: view.getUint32(16),
      height: view.getUint32(20),
      bitDepth: view.getUint8(24),
      colorType: view.getUint8(25),
      compressionMethod: view.getUint8(26),
      filterMethod: view.getUint8(27),
      interlaceMethod: view.getUint8(28),
    };
  }
}

function maekIHDR(data: IHDRData): ArrayBuffer {
  const buf = new ArrayBuffer(25);
  const view = new DataView(buf);
  view.setUint32(0, buf.byteLength - 12);
  view.setUint32(4, ID_IHDR);
  view.setUint32(8, data.width);
  view.setUint32(12, data.height);
  view.setUint8(16, data.bitDepth);
  view.setUint8(17, data.colorType);
  view.setUint8(18, data.compressionMethod);
  view.setUint8(19, data.filterMethod);
  view.setUint8(20, data.interlaceMethod);
  view.setUint32(21, calcCRC(new Uint8Array(buf, 4, buf.byteLength - 8)));
  return buf;
}

function makeacTL(frameNum: number, loop: number): ArrayBuffer {
  const buf = new ArrayBuffer(20);
  const view = new DataView(buf);
  view.setUint32(0, buf.byteLength - 12);
  view.setUint32(4, ID_acTL);
  view.setUint32(8, frameNum);
  view.setUint32(12, loop);
  view.setUint32(16, calcCRC(new Uint8Array(buf, 4, buf.byteLength - 8)));
  return buf;
}

function makefcTL(data: fcTLData): ArrayBuffer {
  const buf = new ArrayBuffer(38);
  const view = new DataView(buf);
  view.setUint32(0, buf.byteLength - 12);
  view.setUint32(4, ID_fcTL);
  view.setUint32(8, data.sequenceNumber);
  view.setUint32(12, data.width);
  view.setUint32(16, data.height);
  view.setUint32(20, data.xOffset);
  view.setUint32(24, data.yOffset);
  view.setUint16(28, data.delayNum);
  view.setUint16(30, data.delayDen);
  view.setUint8(32, data.disposeOp);
  view.setUint8(33, data.blendOp);
  view.setUint32(34, calcCRC(new Uint8Array(buf, 4, buf.byteLength - 8)));
  return buf;
}

function copyIDATChunks(bin: ArrayBuffer, asfdAt: boolean): ArrayBuffer[] {
  const view = new DataView(bin);
  const chunks: ArrayBuffer[] = [];
  let cursor = 8;
  while (cursor < view.byteLength) {
    const size = view.getUint32(cursor);
    const next = cursor + size + 12;
    const type = view.getUint32(cursor + 4);
    if (type === ID_IDAT) {
      if (asfdAt) {
        const dstBuf = new ArrayBuffer(next - cursor + 4);
        const dstView = new DataView(dstBuf);
        dstView.setUint32(0, dstBuf.byteLength - 12);
        dstView.setUint32(4, ID_fdAT);
        const src = new Uint8Array(bin, cursor + 8, next - cursor - 8);
        new Uint8Array(dstBuf, 12).set(src);
        chunks.push(dstBuf);
      } else {
        chunks.push(bin.slice(cursor, next));
      }
    }
    cursor = next;
  }
  return chunks;
}

function writeSecNumForfdATChunk(chunk: ArrayBuffer, secNum: number) {
  const view = new DataView(chunk);
  view.setUint32(8, secNum);
  view.setUint32(
    chunk.byteLength - 4,
    calcCRC(new Uint8Array(chunk, 4, chunk.byteLength - 8))
  );
}

class Encoder {
  private width_: number;
  private height_: number;
  private frames_: Promise<FrameData>[];

  constructor(width: number, height: number) {
    this.width_ = width;
    this.height_ = height;
    this.frames_ = [];
  }

  get width() {
    return this.width_;
  }

  get height() {
    return this.height_;
  }

  async pushImg(
    img: ImageData,
    delayNum: number,
    delayDen: number,
    {
      xOffset = 0,
      yOffset = 0,
      disposeOp = "none",
      blendOp = "source",
    }: Partial<Options> = {}
  ) {
    const canvas = new OffscreenCanvas(img.width, img.height);
    const ctx = canvas.getContext("2d")!;
    ctx.putImageData(img, 0, 0);

    const isFirstFrame = this.frames_.length === 0;
    const promise = canvas
      .convertToBlob({ type: "image/png" })
      .then((blob) => blob.arrayBuffer())
      .then((bin) => ({
        data: copyIDATChunks(bin, !isFirstFrame),
        width: img.width,
        height: img.height,
        delayNum,
        delayDen,
        xOffset,
        yOffset,
        disposeOp: DisposeOp[disposeOp],
        blendOp: BlendOp[blendOp],
      }));
    this.frames_.push(promise);
    return promise.then(() => {});
  }

  async finish(loop: number = 0) {
    const frames = await Promise.all(this.frames_);

    const blobPart: BlobPart[] = [];
    blobPart.push(ID_PNG.buffer);
    blobPart.push(
      maekIHDR({
        width: this.width_,
        height: this.height_,
        bitDepth: 8,
        colorType: 6,
        compressionMethod: 0,
        filterMethod: 0,
        interlaceMethod: 0,
      })
    );

    if (frames.length < 2) {
      blobPart.push(...frames[0].data);
    } else {
      blobPart.push(makeacTL(frames.length, loop));

      let secNum = 0;
      blobPart.push(
        makefcTL({
          sequenceNumber: secNum++,
          width: frames[0].width,
          height: frames[0].height,
          xOffset: frames[0].xOffset,
          yOffset: frames[0].yOffset,
          delayNum: frames[0].delayNum,
          delayDen: frames[0].delayDen,
          disposeOp: frames[0].disposeOp,
          blendOp: frames[0].blendOp,
        })
      );
      blobPart.push(...frames[0].data);

      for (let i = 1; i < frames.length; ++i) {
        blobPart.push(
          makefcTL({
            sequenceNumber: secNum++,
            width: frames[i].width,
            height: frames[i].height,
            xOffset: frames[i].xOffset,
            yOffset: frames[i].yOffset,
            delayNum: frames[i].delayNum,
            delayDen: frames[i].delayDen,
            disposeOp: frames[i].disposeOp,
            blendOp: frames[i].blendOp,
          })
        );
        const frameData = frames[i].data;
        frameData.forEach((frame) => writeSecNumForfdATChunk(frame, secNum++));
        blobPart.push(...frameData);
      }
    }

    blobPart.push(IEND);

    return new Blob(blobPart, { type: "image/png" });
  }

  dispose() {}
}

export default Encoder;
