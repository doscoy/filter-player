import Loader from "./components/Loader.svelte";

const [img] = Array.from(document.body.getElementsByTagName("img"));
const [video] = Array.from(document.body.getElementsByTagName("video"));
if (img) {
  img.crossOrigin = "anonymous";
  new Loader({
    target: document.body,
    props: { mediaEl: img.parentElement!.removeChild(img)! },
  });
} else if (video) {
  video.crossOrigin = "anonymous";
  new Loader({
    target: document.body,
    props: { mediaEl: video.parentElement!.removeChild(video)! },
  });
} else {
  new Loader({
    target: document.body,
  });
}
