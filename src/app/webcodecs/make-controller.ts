import type { VideoFrameController } from "./videoframe";
import { Video, StaticImage, AnimatedImage } from "./videoframe";

async function waitImgLoading(el: HTMLImageElement) {
  if (!el.complete) {
    await new Promise<void>((resolve) => {
      el.addEventListener("load", () => resolve(), { once: true });
    });
  }
  return el;
}

async function waitVideoLoading(el: HTMLVideoElement) {
  el.load();
  await new Promise<void>((resolve) => {
    el.addEventListener("canplaythrough", () => resolve(), { once: true });
  });
  el.loop = true;
  el.muted = true;
  await el.play();
  return el;
}

interface FetchedData {
  type: string;
  data: ReadableStream<Uint8Array>;
}

async function fetchBinary(url: string): Promise<FetchedData> {
  const res = await fetch(url);
  res.body;
  const mime = res.headers.get("Content-Type");
  const data = res.body ? res.body : new ReadableStream<Uint8Array>();
  return { type: mime ? mime : "unknown", data };
}

async function makeFromFetchedImgData(data: FetchedData) {
  const decoder = new ImageDecoder({
    ...data,
    preferAnimation: true,
  });
  await decoder.completed;
  await decoder.tracks.ready;
  const track = decoder.tracks.selectedTrack;
  if (!track) return;
  const firstFrame = (
    await decoder.decode({ frameIndex: 0, completeFramesOnly: true })
  ).image;
  if (!track.animated || track.frameCount === 1) {
    return new StaticImage(firstFrame);
  } else {
    return new AnimatedImage(decoder, firstFrame);
  }
}

async function makeFromImgElm(el: HTMLImageElement) {
  if (window.isSecureContext && ImageDecoder) {
    const data = await fetchBinary(el.src);
    if (await ImageDecoder.isTypeSupported(data.type)) {
      const controller = await makeFromFetchedImgData(data);
      if (controller) return controller;
    }
  }

  el = await waitImgLoading(el);
  return new StaticImage(await createImageBitmap(el));
}

async function makeFromVideoElm(el: HTMLVideoElement) {
  return new Video(await waitVideoLoading(el));
}

async function makeVideoFrameControllerFromElm(
  el: HTMLImageElement | HTMLVideoElement
): Promise<VideoFrameController> {
  if (el instanceof HTMLImageElement) {
    return makeFromImgElm(el);
  } else {
    el.load();
    return makeFromVideoElm(el);
  }
}

async function makeVideoFrameControllerFromFile(
  file: File
): Promise<VideoFrameController> {
  if (file.type.includes("image")) {
    const el = new Image();
    el.src = URL.createObjectURL(file);
    return makeFromImgElm(el);
  } else if (file.type.includes("video")) {
    const el = document.createElement("video");
    el.autoplay = true;
    el.controls = true;
    el.src = URL.createObjectURL(file);
    return makeFromVideoElm(el);
  } else {
    return Promise.reject(`invalid file type "${file.type}"`);
  }
}

export { makeVideoFrameControllerFromElm, makeVideoFrameControllerFromFile };
