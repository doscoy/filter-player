import type {
  VideoFrameController,
  VideoFrameCallback,
  VideoFrameData,
} from "./videoframe";
import { clamp } from "./util";

declare global {
  interface HTMLVideoElement {
    captureStream(): MediaStream;
  }
}

class Video implements VideoFrameController {
  private video_: HTMLVideoElement;
  private frame_: VideoFrame;
  private fps_: number;
  private start_: number;
  private end_: number;
  private cbHandler_: number | null;

  constructor(video: HTMLVideoElement) {
    this.video_ = video;
    {
      const [track] = this.video_.captureStream().getVideoTracks();
      const { frameRate } = track.getSettings();
      this.fps_ = frameRate!;
    }
    this.frame_ = new VideoFrame(this.video_);
    this.start_ = 0;
    this.end_ = this.video_.duration;
    this.cbHandler_ = null;
  }

  get width() {
    return this.video_.videoWidth;
  }

  get height() {
    return this.video_.videoHeight;
  }

  get fps() {
    return this.fps_;
  }

  get srcDuration() {
    return this.video_.duration;
  }

  get duration() {
    return this.end_ - this.start_;
  }

  get currentSrcTime() {
    return this.video_.currentTime;
  }

  get currentTime() {
    return this.video_.currentTime - this.start_;
  }

  get start() {
    return this.start_;
  }

  get end() {
    return this.end_;
  }

  get paused() {
    return this.video_.paused;
  }

  get seekable() {
    const [start, end] = this.getSeekableRange();
    return start < end;
  }

  get muted() {
    return this.video_.muted;
  }

  set muted(muted: boolean) {
    this.video_.muted = muted;
  }

  get volume() {
    return this.video_.volume;
  }

  set volume(volume: number) {
    this.video_.volume = volume;
  }

  private getSeekableRange(): [number, number] {
    const seekable = this.video_.seekable;
    if (!seekable || seekable.length < 1) return [0, 0];
    return [seekable.start(0), seekable.end(0)];
  }

  private async waitPause(): Promise<void> {
    if (this.video_.paused) return;

    const promise = new Promise<void>((resolve) => {
      this.video_.addEventListener("pause", () => resolve(), { once: true });
    });
    this.video_.pause();

    await promise;
  }

  private updateFrame() {
    this.frame_.close();
    this.frame_ = new VideoFrame(this.video_);
  }

  getFrameData(): VideoFrameData {
    return { frame: this.frame_.clone(), timestamp: this.currentTime };
  }

  async setCurrentTime(sec: number): Promise<void> {
    const [start, end] = this.getSeekableRange();
    if (start >= end) return;
    const promise = new Promise<void>((resolve) => {
      this.video_.addEventListener("seeked", () => resolve(), { once: true });
    });
    const min = Math.max(start, this.start_);
    const max = Math.min(end, this.end_);
    this.video_.currentTime = clamp(this.start_ + sec, min, max);
    await promise;
    this.updateFrame();
  }

  async setStart(sec: number): Promise<void> {
    const [start, end] = this.getSeekableRange();
    if (start >= end) return;
    const min = Math.max(start, 0);
    const max = Math.min(end, this.end_ - 1 / this.fps_);
    this.start_ = clamp(sec, min, max);
    if (this.start_ > this.video_.currentTime) {
      await this.setCurrentTime(0);
    }
  }

  async setEnd(sec: number): Promise<void> {
    const [start, end] = this.getSeekableRange();
    if (start >= end) return;
    const min = Math.max(start, this.start_ + 1 / this.fps_);
    const max = Math.min(end, this.video_.duration);
    this.end_ = clamp(sec, min, max);
    if (this.video_.currentTime > this.end_) {
      await this.setCurrentTime(this.end_ - this.start_);
    }
  }

  async advanceFrame(n: number = 1): Promise<boolean> {
    await this.pause();

    if (n === 0) return false;

    const nextIndex = Math.round(this.video_.currentTime * this.fps_) + n;
    const nextTime = Math.round((nextIndex / this.fps_) * 1000000) / 1000000;
    if (n > 0 && this.end_ < nextTime) return false;
    if (n < 0 && nextTime < this.start_) return false;

    const currTime = this.video_.currentTime;
    const promise = new Promise<void>((resolve) => {
      this.video_.requestVideoFrameCallback(() => {
        this.updateFrame();
        resolve();
      });
    });
    this.video_.currentTime = nextTime;
    await promise;

    if (n > 0) {
      return currTime < this.video_.currentTime;
    } else {
      return this.video_.currentTime < currTime;
    }
  }

  async advanceStart(n: number): Promise<boolean> {
    if (n === 0) return false;
    const nextIndex = Math.round(this.start_ * this.fps_) + n;
    const nextTime = Math.round((nextIndex / this.fps_) * 1000000) / 1000000;
    await this.setStart(nextTime);
    return true;
  }

  async advanceEnd(n: number): Promise<boolean> {
    if (n === 0) return false;
    const nextIndex = Math.round(this.end_ * this.fps_) + n;
    const nextTime = Math.round((nextIndex / this.fps_) * 1000000) / 1000000;
    await this.setEnd(nextTime);
    return true;
  }

  play(cb: VideoFrameCallback) {
    const onNewFrame: VideoFrameRequestCallback = async (_now, _metadata) => {
      if (
        this.video_.currentTime >= this.end_ ||
        this.start_ > this.video_.currentTime
      ) {
        await this.setCurrentTime(0);
      }
      this.updateFrame();
      cb(this.getFrameData());
      if (this.cbHandler_ !== null) {
        this.cbHandler_ = this.video_.requestVideoFrameCallback(onNewFrame);
      }
    };
    this.cbHandler_ = this.video_.requestVideoFrameCallback(onNewFrame);
    this.video_.play();
  }

  async pause(): Promise<void> {
    await this.waitPause();
    if (this.cbHandler_ === null) return;
    this.video_.cancelVideoFrameCallback(this.cbHandler_);
    this.cbHandler_ = null;
  }
}

export { Video };
