function clamp(x: number, min: number, max: number) {
  return Math.min(Math.max(min, x), max);
}

export { clamp };
