import type {
  VideoFrameController,
  VideoFrameCallback,
  VideoFrameData,
} from "./videoframe";
import { clamp } from "./util";

class AnimatedImage implements VideoFrameController {
  private decoder_: ImageDecoder;
  private frame_: VideoFrame;
  private frames_: number;
  private start_: number;
  private end_: number;
  private cur_: number;
  private startTS_: number;
  private timeoutID_: number;

  constructor(decoder: ImageDecoder, firstFrame: VideoFrame) {
    this.decoder_ = decoder;
    this.frame_ = firstFrame;
    this.frames_ = this.decoder_.tracks.selectedTrack!.frameCount;
    this.start_ = 0;
    this.end_ = this.frames_;
    this.cur_ = 0;
    this.startTS_ = 0;
    this.timeoutID_ = 0;
  }

  get width(): number {
    return this.frame_.displayWidth;
  }
  get height(): number {
    return this.frame_.displayHeight;
  }

  get fps(): number {
    return this.frame_.duration ? 1_000_000 / this.frame_.duration : 0;
  }

  get srcDuration(): number {
    return this.frames_;
  }
  get duration(): number {
    return this.end_ - this.start_;
  }

  get currentSrcTime(): number {
    return this.cur_;
  }
  get currentTime(): number {
    return this.cur_ - this.start_;
  }

  get start(): number {
    return this.start_;
  }
  get end(): number {
    return this.end_;
  }

  get paused(): boolean {
    return this.timeoutID_ === 0;
  }

  get seekable(): boolean {
    return this.duration > 1;
  }

  private async updateFrame() {
    this.frame_.close();
    this.frame_ = (
      await this.decoder_.decode({
        frameIndex: this.cur_,
        completeFramesOnly: true,
      })
    ).image;
  }

  getFrameData(): VideoFrameData {
    return {
      frame: this.frame_.clone(),
      timestamp: (this.frame_.timestamp - this.startTS_) / 1_000_000,
    };
  }

  async setCurrentTime(frame: number): Promise<void> {
    this.cur_ = clamp(
      Math.floor(frame + this.start_),
      this.start_,
      this.end_ - 1
    );
    await this.updateFrame();
  }

  async setStart(frame: number): Promise<void> {
    this.start_ = clamp(Math.floor(frame), 0, this.end_ - 1);
    if (this.cur_ < this.start_) {
      await this.setCurrentTime(0);
    }
    this.startTS_ = (
      await this.decoder_.decode({
        frameIndex: this.cur_,
        completeFramesOnly: true,
      })
    ).image.timestamp;
  }

  async setEnd(frame: number): Promise<void> {
    this.end_ = clamp(Math.floor(frame), this.start_ + 1, this.frames_);
    if (this.end_ <= this.cur_) {
      await this.setCurrentTime(this.end_ - this.start_);
    }
  }

  async advanceFrame(n: number): Promise<boolean> {
    await this.pause();

    if (n === 0) return false;

    const newCur = this.cur_ + n;
    if (this.start_ <= newCur && newCur < this.end_) {
      this.cur_ = newCur;
      await this.updateFrame();
      return true;
    } else {
      return false;
    }
  }

  async advanceStart(n: number): Promise<boolean> {
    if (n === 0) return false;
    const newStart = this.start_ + n;
    if (0 <= newStart && newStart < this.end_) {
      await this.setStart(newStart);
      return true;
    } else {
      return false;
    }
  }

  async advanceEnd(n: number): Promise<boolean> {
    if (n === 0) return false;
    const newEnd = this.end_ + n;
    if (this.start_ < newEnd && newEnd <= this.frames_) {
      await this.setEnd(newEnd);
      return true;
    } else {
      return false;
    }
  }

  play(cb: VideoFrameCallback): void {
    if (this.timeoutID_ > 0) clearInterval(this.timeoutID_);
    this.timeoutID_ = 0;
    const loop = async () => {
      ++this.cur_;
      if (this.cur_ >= this.end_) this.cur_ = this.start_;
      await this.updateFrame();
      cb(this.getFrameData());
      const dur = this.frame_.duration ? this.frame_.duration / 1000 : 10;
      this.timeoutID_ = setTimeout(loop, dur);
    };
    loop();
  }

  pause(): Promise<void> {
    if (this.timeoutID_ > 0) clearInterval(this.timeoutID_);
    this.timeoutID_ = 0;
    return Promise.resolve();
  }
}

export { AnimatedImage };
