interface VideoFrameData {
  frame: VideoFrame;
  timestamp: number;
}

type VideoFrameCallback = (data: VideoFrameData) => void;

interface VideoFrameController {
  get width(): number;
  get height(): number;

  get fps(): number;

  get srcDuration(): number;
  get duration(): number;

  get currentSrcTime(): number;
  get currentTime(): number;

  get start(): number;
  get end(): number;

  get paused(): boolean;
  get seekable(): boolean;

  getFrameData(): VideoFrameData;

  setCurrentTime(sec: number): Promise<void>;
  setStart(sec: number): Promise<void>;
  setEnd(sec: number): Promise<void>;
  advanceFrame(n: number): Promise<boolean>;
  advanceStart(n: number): Promise<boolean>;
  advanceEnd(n: number): Promise<boolean>;

  play(cb: VideoFrameCallback): void;
  pause(): Promise<void>;
}

import { StaticImage } from "./static-image";
import { AnimatedImage } from "./animated-image";
import { Video } from "./video";

export type { VideoFrameController, VideoFrameCallback, VideoFrameData };
export { Video, StaticImage, AnimatedImage };
