import type {
  VideoFrameController,
  VideoFrameCallback,
  VideoFrameData,
} from "./videoframe";
import { clamp } from "./util";

class StaticImage implements VideoFrameController {
  private frame_: VideoFrame;
  private fps_: number;
  private frames_: number;
  private cur_: number;
  private intervalID_: number;
  private callback_: VideoFrameCallback | null;

  constructor(frame: VideoFrame | ImageBitmap) {
    this.frame_ =
      frame instanceof VideoFrame
        ? frame
        : new VideoFrame(frame, { timestamp: 0 });
    this.fps_ = 0;
    this.frames_ = 1;
    this.cur_ = 0;
    this.intervalID_ = 0;
    this.callback_ = null;
  }

  get width(): number {
    return this.frame_.displayWidth;
  }
  get height(): number {
    return this.frame_.displayHeight;
  }

  get fps(): number {
    return this.fps_;
  }

  get frames(): number {
    return this.frames_;
  }

  get srcDuration(): number {
    return this.frames_;
  }
  get duration(): number {
    return this.srcDuration;
  }

  get currentSrcTime(): number {
    return this.cur_;
  }
  get currentTime(): number {
    return this.currentSrcTime;
  }

  get start(): number {
    return 0;
  }
  get end(): number {
    return this.srcDuration;
  }

  get paused(): boolean {
    return this.intervalID_ === 0;
  }

  get seekable(): boolean {
    return false;
  }

  get isPlayable(): boolean {
    return this.fps_ > 0 && this.frames_ > 1;
  }

  getFrameData(): VideoFrameData {
    return { frame: this.frame_, timestamp: this.cur_ / this.fps_ };
  }

  setDuration(fps: number, frames: number) {
    if (this.intervalID_) clearInterval(this.intervalID_);
    this.fps_ = Math.max(fps, 0);
    this.frames_ = Math.max(frames, 1);
    this.cur_ = Math.min(this.cur_, this.frames_);
    if (this.intervalID_) {
      this.intervalID_ = 0;
      this.play(this.callback_!);
    }
  }

  setCurrentTime(frame: number): Promise<void> {
    this.cur_ = clamp(Math.floor(frame), this.start, this.end - 1);
    return Promise.resolve();
  }

  setStart(_frame: number): Promise<void> {
    return Promise.resolve();
  }

  setEnd(_frame: number): Promise<void> {
    return Promise.resolve();
  }

  async advanceFrame(n: number): Promise<boolean> {
    await this.pause();

    if (n === 0) return false;

    const newCur = this.cur_ + n;
    if (this.start <= newCur && newCur < this.end) {
      this.cur_ = newCur;
      return true;
    } else {
      return false;
    }
  }

  advanceStart(_: number): Promise<boolean> {
    return Promise.resolve(false);
  }

  advanceEnd(_: number): Promise<boolean> {
    return Promise.resolve(false);
  }

  play(cb: VideoFrameCallback): void {
    if (this.fps_ < 1 || this.frames_ < 2) {
      cb(this.getFrameData());
      return;
    }
    if (this.intervalID_) clearInterval(this.intervalID_);
    this.callback_ = cb;
    this.intervalID_ = setInterval(() => {
      this.cur_ = (this.cur_ + 1) % this.frames_;
      this.callback_!(this.getFrameData());
    }, 1000 / this.fps_);
  }

  pause(): Promise<void> {
    if (this.intervalID_) clearInterval(this.intervalID_);
    this.intervalID_ = 0;
    this.callback_ = null;
    return Promise.resolve();
  }
}

export { StaticImage };
