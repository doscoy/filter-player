// @ts-ignore
import LoaderScript from "../app/loader?script";

// @ts-ignore
import StyleScript from "../app/style?script&module";

import InitData from "../init.json";

const injected = new Set<number>();

async function getCurrentTab(): Promise<chrome.tabs.Tab | undefined> {
  const [tab] = await chrome.tabs.query({
    active: true,
    currentWindow: true,
  });
  return tab;
}

chrome.runtime.onInstalled.addListener((detail) => {
  if (detail.reason === "install") {
    chrome.storage.local.clear().then(() => {
      chrome.storage.local.set(InitData);
    });
  }

  chrome.contextMenus.create({
    title: "launch editor",
    id: "launch",
    documentUrlPatterns: ["*://*/*"],
    contexts: ["image", "video"],
  });
});

chrome.contextMenus.onClicked.addListener(async (info) => {
  if (!info.pageUrl || !info.srcUrl) return;

  const tab = await getCurrentTab();
  if (!tab || !tab.id || injected.has(tab.id)) return;

  if (info.pageUrl === info.srcUrl) {
    const frameIds = info.frameId !== undefined ? [info.frameId] : undefined;
    chrome.scripting.executeScript({
      target: { tabId: tab.id, frameIds },
      files: [StyleScript, LoaderScript],
    });
    injected.add(tab.id);
  } else {
    chrome.tabs.create({
      url: info.srcUrl?.split("?")[0],
      index: tab.index + 1,
      openerTabId: tab.id,
    });
  }
});

chrome.tabs.onRemoved.addListener((tabId) => injected.delete(tabId));
chrome.tabs.onUpdated.addListener((tabId) => injected.delete(tabId));

chrome.action.onClicked.addListener(() => {
  chrome.tabs.create({
    url: chrome.runtime.getURL("src/app/index.html"),
  });
});
