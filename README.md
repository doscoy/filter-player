# filter player

HTMLImageElement と HTMLVideoElement で表示できるメディアを wgsl で加工して png, gif, webp として保存する Chrome extensions

![screenshot](screenshot/rec.mp4)

## build

```sh
npm install -f
npm run build
```

## install

上記ビルドコマンドで生成した`build`フォルダを[Chrome でロードする](https://developer.chrome.com/docs/extensions/mv3/getstarted/development-basics/#load-unpacked)
