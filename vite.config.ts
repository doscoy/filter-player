import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";
import { crx } from "@crxjs/vite-plugin";
import manifest from "./manifest.config";

import { vitePreprocess } from "@sveltejs/vite-plugin-svelte";
import { optimizeImports, elements } from "carbon-preprocess-svelte";
import cssInjectedByJsPlugin from "vite-plugin-css-injected-by-js";

// https://vitejs.dev/config/
export default defineConfig(({ command }) => ({
  plugins: [
    svelte({
      preprocess: [vitePreprocess(), optimizeImports(), elements()],
    }),
    crx({ manifest }),
    cssInjectedByJsPlugin({
      jsAssetsFilterFunction(outputChunk) {
        return outputChunk.fileName.includes("style.ts");
      },
    }),
  ],
  build: {
    rollupOptions: {
      input: {
        app: "src/app/index.html",
      },
    },
    outDir: command === "build" ? "build" : "dev",
  },
}));
